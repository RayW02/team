package com.cpt202.team.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cpt202.team.models.Team;

//Spring annotation
@RestController
@RequestMapping("/team")
public class TeamController {
    private List<Team> teams = new ArrayList<Team>();
  
    //Get a request"", excute this method 
    @GetMapping("/list")
    public List<Team> getList(){
        return teams;
    }

    @PostMapping("/add")
    public void addTeam(@RequestBody Team team){
        teams.add(team);
    }
    //@RequestBody: convert a string in a JSON format into object
}
